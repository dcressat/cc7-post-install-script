# CC7 Post-install script
The CC7 Post-install script takes care of:
* Adding date and time stamping to the root user history file in order to more easily keep track of what was done and when by any user having executed any sudo commands.
* Installing and configuring EOS and CVMFS
* Authentication with SSSD, which dispenses from having to manually add students logging to the computer using the 'addusercern` command.
* Installing the KDE desktop environment which is a more modern, user friendlier and more productive desktop environments than Gnome
* Installing the evolution mail client whi Here's a tch with with the EWS extension provide a better email client and easier to setup than Thunderbird and provides the CERN "Exchange Global address list"
* Installing Chromium, screen, terminator, wget, lnav and more
* Running yum update to install any outstanding updates

## How to use it
Once CC7 has been installed on a computer, execute the following command as root:
```
curl -L https://gitlab.cern.ch/dcressat/cc7-post-install-script/-/raw/master/CC7-post-install.sh?inline=false -o /tmp/CC7-post-install.sh && chmod 755 /tmp/CC7-post-install.sh && /tmp/CC7-post-install.sh 2>&1 |tee /tmp/CC7-post-install.sh.log && reboot

```

```
#!/bin/bash
# set flag to exit the script on any error
# set -e
https://gitlab.cern.ch/dcressat/cc7-post-install-script/-/raw/
echo "########################"
echo "Adding timestamp to user root bash history"
echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile

echo "########################"
echo "Running yum update"
yum -y update

echo "########################"
echo "Installing cvmfs"
yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum -y install cvmfs-fuse3.x86_64
locmap --enable cvmfs
locmap --configure cvmfs

echo "########################"
echo "Configuring /etc/cvmfs/default.local"
cat <<EOF_CVMFS_Config> /etc/cvmfs/default.local
# cvmfs default.local file installed with puppet
# this files overrides and extends the values contained
# within the default.conf file.

CVMFS_QUOTA_LIMIT='20000'
CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128"
CVMFS_CACHE_BASE='/var/lib/cvmfs'
CVMFS_REPOSITORIES='alice-ocdb.cern.ch,alice.cern.ch,ams.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas.cern.ch,bbp.epfl.ch,cms.cern.ch,geant4.cern.ch,ilc.desy.de,lhcb.cern.ch,na61.cern.ch,sft.cern.ch'
EOF_CVMFS_Config

echo "########################"
echo "Restarting autofs to check that CVMFS works"
systemctl restart autofs.service
echo "########################"
echo "Checking that CVMFS works"
cvmfs_config probe

echo "########################"
echo "Enabling EOS"
locmap --enable eosclient
locmap --configure eosclient

echo "########################"
echo "Setting up authentication with SSSD / LDAP"
wget http://linux.web.cern.ch/docs/sssd.conf.example -O /etc/sssd/sssd.conf
chown root:root /etc/sssd/sssd.conf
chmod 0600 /etc/sssd/sssd.conf
restorecon /etc/sssd/sssd.conf
authconfig --enablesssd --enablesssdauth --update
systemctl enable sssd
systemctl start sssd

echo "########################"
echo "Enabling RPM Fusion to support the installation of non-free software"
yum -y localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm

echo "########################"
echo "Installing the K Destop environment and Cinnamon Destop environment"
yum -y groupinstall "KDE Plasma Workspaces"
yum -y install kate konsole

echo "########################"
echo "Installing the Remmina RDP client"
yum -y install nux-dextop-release
yum -y install remmina-plugins-rdp.x86_64

echo "########################"
echo "Installing the Sublime text editor"
rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
yum -y install sublime-text

echo "########################"
echo "Installing java open jdk"
yum -y install java-1.8.0-openjdk-devel.x86_64

echo "########################"
echo "Installing additional software like htop screen, mc, the evolution mail client support for Exchange"
yum -y install evolution-ews evolution-ews-langpacks mc lnav terminator chromium screen wget chromium htop
```
